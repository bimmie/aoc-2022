# AoC-2022

Advent of code 2022 solutions in Haskell

## Getting started

* [GHCUp](https://www.haskell.org/ghcup/)

* GHC 9.4.2

* Cabal

### Running

Build:
```shell
cabal build
```

Run:
```shell
cabal run
```

## TODO:
- Tests
- Command
module Main where

import qualified Day1 (solution1, solution2)
import qualified Day2 (solution1, solution2)
import qualified Day3 (solution1, solution2)
import qualified Day4 (solution1, solution2)
import qualified Day5 (solution1, solution2)
import qualified Day6 (solution1, solution2)
import qualified Day7 (solution1, solution2)

import qualified Data.Text.IO as TIO
import Options.Applicative
import Data.Semigroup ((<>))

data Options = Options
  { day :: Int
  , input :: String
  }

parser :: Parser Options
parser = Options
  <$> argument auto (metavar "DAY")
  <*> argument str (metavar "FILE")

options = info (parser <**> helper)
  (fullDesc
  <> progDesc "Advent of Code 2022 solution")

main :: IO ()
main = do
  p <- execParser options
  x <- TIO.readFile (input p)
  case day p of
    1 -> do
      print $ Day1.solution1 x
      print $ Day1.solution2 x
    2 -> do
      print $ Day2.solution1 x
      print $ Day2.solution2 x
    3 -> do
      print $ Day3.solution1 x
      print $ Day3.solution2 x
    4 -> do
      print $ Day4.solution1 x
      print $ Day4.solution2 x
    5 -> do
      print $ Day5.solution1 x
      print $ Day5.solution2 x
    6 -> do
      print $ Day6.solution1 x
      print $ Day6.solution2 x
    7 -> do
      print $ Day7.solution1 x
      print $ Day7.solution2 x
    _ -> return ()
  
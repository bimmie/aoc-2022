module Day4 (solution1, solution2) where

import qualified Data.Text as T
import Data.Void
import Data.Either (rights)
import Text.Megaparsec (Parsec, parse)
import Text.Megaparsec.Char (char)
import Text.Megaparsec.Char.Lexer (decimal)

type Parser = Parsec Void T.Text

data Range = Range Int Int    -- Range lo hi (with lo <= hi)
               deriving (Show)

inside :: (Range, Range) -> Bool  -- is either range fully inside the other?
inside (Range lo1 hi1, Range lo2 hi2) = lo1 <= lo2 && hi2 <= hi1
                                     || lo2 <= lo1 && hi1 <= hi2

overlapping :: (Range, Range) -> Bool  -- does either range overlap with the other?
overlapping (Range lo1 hi1, Range lo2 hi2) = lo1 <= lo2 && lo2 <= hi1
                                          || lo2 <= lo1 && lo1 <= hi2

rangeParser :: Parser Range
rangeParser = do
    lo <- decimal
    _ <- char '-'
    hi <- decimal
    return $ Range lo hi

assignmentParser :: Parser (Range, Range)
assignmentParser = do
    r1 <- rangeParser
    _ <- char ','
    r2 <- rangeParser
    return (r1, r2)

solution1 :: T.Text -> Int
solution1 input = length $ filter inside $ rights $ map (parse assignmentParser "") $ T.lines input

solution2 :: T.Text -> Int
solution2 input = length $ filter overlapping $ rights $ map (parse assignmentParser "") $ T.lines input

module Day1 (solution1, solution2) where

import qualified Data.Text as T
import Data.Text.Read (decimal)
import Data.Either (rights)
import Data.List (sort)

solution1 :: T.Text -> Int
solution1 input =
  maximum $ caloriesPerElf input

solution2 :: T.Text -> Int
solution2 input =
  sum $ take 3 $ reverse $ sort $ caloriesPerElf input

textToInt :: [T.Text] -> [Int]
textToInt = (map fst) . rights . (map decimal)

caloriesPerElf :: T.Text -> [Int]
caloriesPerElf input =
  let
    items = map T.lines $ T.splitOn "\n\n" input
  in
    map (sum . textToInt) items
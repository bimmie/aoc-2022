module Day7 (solution1, solution2) where

import qualified Data.Text as T
import Data.Void
import Data.Either
import Data.List (foldl', break, concat)
import Text.Megaparsec (Parsec, many, parse, (<|>))
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer (decimal)

--
-- Parsing the commands
--

type Parser = Parsec Void T.Text

data FSEntry = File T.Text Int       -- filename filesize
             | Dir T.Text [FSEntry]  -- dirname contents
             deriving (Show)

data Command = Ls [FSEntry]
             | Cd T.Text             -- dirname
             deriving (Show)

commandsParser :: Parser [Command]
commandsParser = many (cdParser <|> lsParser)

lsParser :: Parser Command
lsParser = do
    _ <- string "$ ls\n"
    entries <- many (dirEntryParser <|> fileEntryParser)
    pure $ Ls entries

nameParser :: Parser String
nameParser = many (alphaNumChar <|> char '/' <|> char '.')

cdParser :: Parser Command
cdParser = do
    _ <- string "$ cd "
    dirname <- nameParser
    _ <- char '\n'
    pure $ Cd (T.pack dirname)

fileEntryParser :: Parser FSEntry
fileEntryParser = do
    size <- decimal
    _ <- char ' '
    name <- nameParser
    _ <- char '\n'
    pure $ File (T.pack name) size

dirEntryParser :: Parser FSEntry
dirEntryParser = do
    _ <- string "dir "
    name <- many alphaNumChar
    _ <- char '\n'
    pure $ Dir (T.pack name) []

--
-- File system tree operations
--

data FSCrumb = FSCrumb T.Text [FSEntry] [FSEntry]
                 deriving (Show)

type FSZipper = (FSEntry, [FSCrumb])

mkEmptyFs :: FSEntry
mkEmptyFs = Dir "/" []

fsNewEntry :: FSEntry -> FSZipper -> FSZipper
fsNewEntry entry (Dir name entries, bs) = (Dir name (entry:entries), bs)

fsUp :: FSZipper -> FSZipper
fsUp (entry, FSCrumb name ls rs : bs) = (Dir name (ls ++ [entry] ++ rs), bs)

fsRoot :: FSZipper -> FSEntry
fsRoot (entry, []) = entry
fsRoot zipper = fsRoot $ fsUp zipper

fsInto :: T.Text -> FSZipper -> FSZipper
fsInto name (Dir dirName entries, bs) = (entry, FSCrumb dirName ls rs : bs)
  where
    (ls, entry:rs) = break (nameIs name) entries

fsChangeDir :: T.Text -> FSZipper -> FSZipper
fsChangeDir name zipper
  | name == "/" = (fsRoot zipper, [])
  | name == ".." = fsUp zipper
  | otherwise = fsInto name zipper

nameIs :: T.Text -> FSEntry -> Bool
nameIs name (Dir dirName _) = name == dirName
nameIs name (File fileName _) = name == fileName

size :: FSEntry -> Int
size (File _ sz) = sz
size (Dir _ []) = 0
size (Dir _ entries) = sum $ size <$> entries

--
-- Building the filesystem contents
--

buildFileSystem :: [Command] -> FSEntry
buildFileSystem commands = fsRoot $ exec commands $ (mkEmptyFs, [])
  where
    exec :: [Command] -> FSZipper -> FSZipper
    exec [] z = z
    exec ((Cd name) : commands) z = exec commands (fsChangeDir name z)
    exec ((Ls entries) : commands) z = exec commands (foldl' (flip fsNewEntry) z entries)

--
-- Solution
--

visitDirs :: (FSEntry -> a) -> [a] -> FSEntry -> [a]
visitDirs _ acc (File _ _) = acc
visitDirs f acc dir@(Dir _ entries) = (f dir) : (concat $ map (visitDirs f acc) entries) 

dirSizes :: FSEntry -> [Int]
dirSizes entry = visitDirs size [] entry

parseFs :: T.Text -> FSEntry
parseFs input = buildFileSystem $ fromRight [] $ parse commandsParser "" input

solution1 :: T.Text -> Int
solution1 input = sum $ filter (<= 100000) $ dirSizes $ parseFs input 

solution2 :: T.Text -> Int
solution2 input = minimum $ filter (>= needed) $ dirSizes $ root
  where
    root = parseFs input
    used = size root
    unused = 70000000 - used
    needed = 30000000 - unused

module Day6 (solution1, solution2) where

import Data.List (nub)
import qualified Data.Text as T

allDifferent :: (Int, T.Text) -> Bool
allDifferent (_, s) = length (T.unpack s) == length (nub $ T.unpack s)

markerStart :: Int -> T.Text -> Int
markerStart size message = fst $ head $ filter allDifferent (zip [size ..] (T.take size <$> T.tails message))

solution1 :: T.Text -> Int
solution1 = markerStart 4

solution2 :: T.Text -> Int
solution2 = markerStart 14
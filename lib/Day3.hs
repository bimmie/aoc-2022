module Day3 (solution1, solution2) where

import qualified Data.Text as T
import Data.Maybe (mapMaybe)
import Data.List (intersect, nub, foldl1')
import Data.List.Split (chunksOf)

priority :: Char -> Maybe Int
priority item = lookup item priorities
  where
    priorities = zip ['a' .. 'z'] [1 .. 26]
              ++ zip ['A' .. 'Z'] [27 .. 52]
  
commonItems :: [T.Text] -> [Char]  -- find items that rucksacks or compartments have in common
commonItems items = T.unpack $ foldl1' (\items1 items2 -> T.pack $ common items1 items2) items
  where
    common :: T.Text -> T.Text -> [Char]
    common items1 items2 = nub $ intersect (T.unpack items1) (T.unpack items2)

splitRucksack :: T.Text -> [T.Text]  -- split rucksack into compartments
splitRucksack rucksack = T.chunksOf (T.length rucksack `div` 2) rucksack

solution1 :: T.Text -> Int
solution1 text = sum $ concatMap (mapMaybe priority . commonItems) $ map splitRucksack $ T.lines text

solution2 :: T.Text -> Int
solution2 text = sum $ concatMap (mapMaybe priority . commonItems) $ chunksOf 3 $ T.lines text

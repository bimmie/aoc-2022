module Day2 (solution1, solution2) where

import qualified Data.Text as T
import Data.Maybe (mapMaybe)

data RPS
  = Rock
  | Paper
  | Scissors
  deriving (Show)

data Outcome
  = Win
  | Draw
  | Loss
  deriving (Eq)

rpsScore :: RPS -> Int
rpsScore Rock = 1
rpsScore Paper = 2
rpsScore Scissors = 3

outcomeScore :: Outcome -> Int
outcomeScore Win = 6
outcomeScore Draw = 3
outcomeScore Loss = 0

score :: RPS -> RPS -> Int   -- a is the opponent, b is us
score a b = (outcomeScore $ outcome a b) + (rpsScore b)

outcome :: RPS -> RPS -> Outcome
outcome Rock Rock = Draw
outcome Rock Paper = Win
outcome Rock Scissors = Loss
outcome Paper Rock = Loss
outcome Paper Paper = Draw
outcome Paper Scissors = Win
outcome Scissors Rock = Win
outcome Scissors Paper = Loss
outcome Scissors Scissors = Draw

outcome' :: RPS -> Outcome -> RPS
outcome' Rock Draw = Rock
outcome' Rock Win = Paper
outcome' Rock Loss = Scissors
outcome' Paper Loss = Rock
outcome' Paper Draw = Paper
outcome' Paper Win = Scissors
outcome' Scissors Win = Rock
outcome' Scissors Loss = Paper
outcome' Scissors Draw = Scissors

solution1 :: T.Text -> Int
solution1 text =
  sum $ map (uncurry score) $ mapMaybe parseRound $ T.lines text

solution2 :: T.Text -> Int
solution2 text = 
  sum $ map (\(a, outc) -> score a $ outcome' a outc) $ mapMaybe parseRound' $ T.lines text

parsePlay :: T.Text -> Maybe RPS
parsePlay "A" = Just Rock
parsePlay "B" = Just Paper
parsePlay "C" = Just Scissors
parsePlay "X" = Just Rock
parsePlay "Y" = Just Paper
parsePlay "Z" = Just Scissors
parsePlay _ = Nothing

parseOutcome :: T.Text -> Maybe Outcome
parseOutcome "X" = Just Loss
parseOutcome "Y" = Just Draw
parseOutcome "Z" = Just Win
parseOutcome _ = Nothing

parseRound :: T.Text -> Maybe (RPS, RPS)
parseRound r = case T.split (== ' ') r of
  [a, b] -> parsePlay a >>= \a' -> parsePlay b >>= \b' -> Just (a', b')
  _ -> Nothing

parseRound' :: T.Text -> Maybe (RPS, Outcome)
parseRound' r = case T.split (== ' ') r of
  [a, b] -> parsePlay a >>= \a' -> parseOutcome b >>= \b' -> Just (a', b')
  _ -> Nothing

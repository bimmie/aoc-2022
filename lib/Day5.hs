module Day5 (solution1, solution2) where

import qualified Data.Text as T
import Data.Void
import Data.List (transpose, foldl')
import Data.Maybe (catMaybes, fromMaybe)
import Text.Megaparsec (Parsec, parseMaybe, (<|>), sepBy1)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer (decimal)

type Parser = Parsec Void T.Text

type Stack = [Char]
type Move = (Int, Int, Int)

stackFilledElement :: Parser (Maybe Char)
stackFilledElement = do
  _ <- char '['
  element <- upperChar
  _ <- char ']'
  pure (Just element)

stackEmptyElement :: Parser (Maybe Char)
stackEmptyElement = do
  _ <- string "   " 
  pure Nothing

stackElement :: Parser (Maybe Char)
stackElement = stackFilledElement <|> stackEmptyElement
  
stackRowParser :: Parser [Maybe Char]
stackRowParser = stackElement `sepBy1` char ' '

stackParser :: Parser [Stack]
stackParser = do
  rows <- stackRowParser `sepBy1` char '\n'
  pure $ map catMaybes $ transpose rows

moveParser :: Parser [Move]
moveParser =
  singleMoveParser `sepBy1` char '\n'
  where
    singleMoveParser :: Parser Move
    singleMoveParser = do
      _ <- string "move "
      x <- decimal
      _ <- string " from "
      y <- decimal
      _ <- string " to "
      z <- decimal
      pure (x, y, z)

interpretMoves9001 :: [Stack] -> [Move] -> [Stack]
interpretMoves9001 = foldl' interpretMove9001

interpretMove9001 :: [Stack] -> Move -> [Stack]
interpretMove9001 stacks (number, from, to) = makeMove number from to stacks

interpretMoves :: [Stack] -> [Move] -> [Stack]
interpretMoves = foldl' interpretMove

interpretMove :: [Stack] -> Move -> [Stack]
interpretMove stacks (number, from, to) =
  iterate (makeMove 1 from to) stacks !! number

makeMove :: Int -> Int -> Int -> [Stack] -> [Stack]
makeMove number from to stacks = do
  (idx, stack) <- zip [1..] stacks
  if idx == from then
    pure $ drop number stack
  else if idx == to then
    let crates = take number $ stacks !! (from - 1) in
    pure $ crates ++ stack
  else
    pure stack

solution1 :: T.Text -> T.Text
solution1 input = case T.splitOn "\n\n" input of
  [stacks, moves] -> fromMaybe "broken" (parseMaybe stackParser (dropLastLine stacks)
                        >>= \s -> parseMaybe moveParser moves
                        >>= \m -> Just $ T.pack (map head (interpretMoves s m)))
  _ -> ""

solution2 :: T.Text -> T.Text
solution2 input = case T.splitOn "\n\n" input of
  [stacks, moves] -> fromMaybe "broken" (parseMaybe stackParser (dropLastLine stacks)
                        >>= \s -> parseMaybe moveParser moves
                        >>= \m -> Just $ T.pack (map head (interpretMoves9001 s m)))
  _ -> ""

dropLastLine :: T.Text -> T.Text
dropLastLine = T.intercalate "\n" . reverse . drop 1 . reverse . T.lines
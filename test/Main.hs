module Main (main) where

import Control.Monad (when)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Day1 (solution1, solution2)
import qualified Day2 (solution1, solution2)
import qualified Day3 (solution1, solution2)
import qualified Day4 (solution1, solution2)
import qualified Day5 (solution1, solution2)
import qualified Day6 (solution1, solution2)
import qualified Day7 (solution1, solution2)
import System.Exit
import Test.HUnit

main :: IO ()
main = do
  results <- runTestTT tests
  when (errors results + failures results > 0) $ exitWith (ExitFailure 1)
  exitWith ExitSuccess

test1 =
  TestCase
    ( do
        input <- TIO.readFile "test/example1.txt"
        assertEqual "Day 1 example 1" (Day1.solution1 input) 24000
    )

test2 =
  TestCase
    ( do
        input <- TIO.readFile "test/example1.txt"
        assertEqual "Day 1 example 2" (Day1.solution2 input) 45000
    )

test3 =
  TestCase
    ( do
        input <- TIO.readFile "test/example2.txt"
        assertEqual "Day 2 example 1" (Day2.solution1 input) 15
    )

test4 =
  TestCase
    ( do
        input <- TIO.readFile "test/example2.txt"
        assertEqual "Day 2 example 2" (Day2.solution2 input) 12
    )

test5 =
  TestCase
    ( do
        input <- TIO.readFile "test/example3.txt"
        assertEqual "Day 3 example 1" (Day3.solution1 input) 157
    )

test6 =
  TestCase
    ( do
        input <- TIO.readFile "test/example3.txt"
        assertEqual "Day 3 example 2" (Day3.solution2 input) 70
    )

test7 =
  TestCase
    ( do
        input <- TIO.readFile "test/example4.txt"
        assertEqual "Day 4 example 1" (Day4.solution1 input) 2
    )

test8 =
  TestCase
    ( do
        input <- TIO.readFile "test/example4.txt"
        assertEqual "Day 4 example 2" (Day4.solution2 input) 4
    )

test9 =
  TestCase
    ( do
        input <- TIO.readFile "test/example5.txt"
        assertEqual "Day 5 example 1" (Day5.solution1 input) "CMZ"
    )

test10 =
  TestCase
    ( do
        input <- TIO.readFile "test/example5.txt"
        assertEqual "Day 5 example 2" (Day5.solution2 input) "MCD"
    )

test11 =
  TestCase
    ( do
        input <- TIO.readFile "test/example6.txt"
        assertEqual "Day 6 example 1" (Day6.solution1 input) 11
    )

test12 =
  TestCase
    ( do
        input <- TIO.readFile "test/example6.txt"
        assertEqual "Day 6 example 2" (Day6.solution2 input) 26
    )

test13 =
  TestCase
    ( do
        input <- TIO.readFile "test/example7.txt"
        assertEqual "Day 7 example 1" (Day7.solution1 input) 95437
    )

test14 =
  TestCase
    ( do
        input <- TIO.readFile "test/example7.txt"
        assertEqual "Day 7 example 2" (Day7.solution2 input) 24933642
    )

tests =
  TestList
    [ test1,
      test2,
      test3,
      test4,
      test5,
      test6,
      test7,
      test8,
      test9,
      test10,
      test11,
      test12,
      test13,
      test14  
    ]